const block = document.getElementById('tank');
let left = 400;
let t = 290;

document.onkeydown = function(event) {
  console.log(event);

  if(event.key == 'ArrowRight') {
    block.style.left = left + 'px';
    left = left + 10;
  }

  if(event.key == 'ArrowDown') {
    block.style.top = t + 'px';
    t = t + 10;
  }

  if(event.key == 'ArrowUp') {
    block.style.top = t + 'px';
    t = t - 10;
  }

  if(event.key == 'ArrowLeft') {
    block.style.left = left + 'px';
    left = left - 10;
  }
}

document.onkeyup = function(event) {
  console.log('Отпустили клавишу');
}